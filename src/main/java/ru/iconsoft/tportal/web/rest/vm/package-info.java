/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.iconsoft.tportal.web.rest.vm;
