package ru.iconsoft.tportal.domain.enumeration;

/**
 * The CategoryStatus enumeration.
 */
public enum CategoryStatus {
    AVAILABLE, RESTRICTED, DISABLED
}
